package Util;


import caroco.Data;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class FormataData {
    public static Data tranformaTimestampParaData(Timestamp timestamp) {
        Data auxData = new Data(inverteData(timestamp.toString().substring(0, 10)));
        return auxData;
    }
    
    public static Data tranformaParaData(String data) {
        Data auxData = new Data(data);
        return auxData;
    }

    public static String inverteData(String data) {
        String vet[] = data.split("-");

        String dia = (vet[2]);
        String mes = (vet[1]);
        String ano = (vet[0]);
        
        return dia+"/"+mes+"/"+ano;
    }
    
    public static Data tranformaDateEmData(Date date) {
        if(date == null){
            return null;
        }
        String vet[] = date.toString().split("-");

        String dia = (vet[2]);
        String mes = (vet[1]);
        String ano = (vet[0]);
        
        Data data = new Data(dia+"/"+mes+"/"+ano);
        return data;
    }

    
    
   

    public static String formataUTF8paraData(String dataHora) { // data hora vem do Funcoes.getDataHora()
        return dataHora.substring(0, 16).replaceAll("T", " ");
    }

    public static Timestamp formataDataParaTimestamp(String data) throws ParseException {
        String dataHora = data;
        Timestamp timestamp = null;
        String subHora = data.substring(11, 13);
        try {
            SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            Calendar c = Calendar.getInstance();
            c.setTime(formatoData.parse(dataHora));
            if (subHora.equals("12")) {
                timestamp = new Timestamp(c.getTimeInMillis() + 43200000);
            } else {
                timestamp = new Timestamp(c.getTimeInMillis());
            }
        } catch (Exception e) {
        }
        return timestamp;
    }
    
    public static String formataTimestampParaUTF8(Timestamp data) throws ParseException {
        String dataHora = String.valueOf(data);
        dataHora = dataHora.replace(" ", "T").replace(".0", "");
        dataHora = dataHora + "-02:00";
        return dataHora;
    }

    public static String formataParaDataHoraVisual(Timestamp data) throws ParseException {
        String correta = "";
        try {
            String dataErrada = String.valueOf(data);
            String horaCorreta = dataErrada.substring(10, 19);// pega data
            String dataCorreta = dataErrada.substring(8, 10) + "/";// pega o dia
            dataCorreta = dataCorreta + dataErrada.substring(5, 7) + "/"; // pega o mes
            dataCorreta = dataCorreta + dataErrada.substring(0, 4) + "";// pega o ano
            correta = dataCorreta + horaCorreta;
        } catch (Exception e) {
        }
        return correta;
    }
    
    public static String formataHoraVisualParaTimestamp(String data) throws ParseException {
        String correta = "";
        try {
            String horaCorreta = data.substring(10, 19);// pega data
            String dataCorreta[] = data.substring(0, 10).split("/");
            correta = dataCorreta[2]+"-"+dataCorreta[1]+"-"+dataCorreta[0] + horaCorreta;
        } catch (Exception e) {
        }
        return correta;
    }
    
    public static String formataParaDataHoraVisual(String dataHora) throws ParseException {
        String Correta = "";
        try {
            String dataErrada = dataHora;
            String horaCorreta = dataErrada.substring(10, 19);// pega data
            String dataCorreta = dataErrada.substring(8, 10) + "/";// pega o dia
            dataCorreta = dataCorreta + dataErrada.substring(5, 7) + "/"; // pega o mes
            dataCorreta = dataCorreta + dataErrada.substring(0, 4) + "";// pega o ano
            Correta = dataCorreta + horaCorreta;
        } catch (Exception e) {
        }
        return Correta;
    }
    
    public static String formataUTF8ParaHoraVisual(String utf) throws ParseException {
        String Correta = "";
        try {
            String dataErrada = String.valueOf(utf);
            String horaCorreta = dataErrada.substring(12, 20);// pega data
            String dataCorreta = dataErrada.substring(9, 11) + "/";// pega o dia
            dataCorreta = dataCorreta + dataErrada.substring(6, 8) + "/"; // pega o mes
            dataCorreta = dataCorreta + dataErrada.substring(1, 5) + "";// pega o ano
            Correta = dataCorreta +" "+ horaCorreta;
        } catch (Exception e) {
        }
        return Correta;
    }

    public static String tempoDecorrido(int tempo[], int posicao) {
        boolean ano = tempo[0] == 0 ? false : true;
        boolean mes = tempo[1] == 0 ? false : true;
        boolean dia = tempo[2] == 0 ? false : true;
        String resultado = "";
        if (ano) {
            resultado = resultado + tempo[0] + (tempo[0] > 1 ? " anos " : " ano ");
        }
        if(posicao == 1){
            return resultado;
        }
        if (mes) {
            resultado = resultado + tempo[1] + (tempo[1] > 1 ? " meses " : " mês ");
        }
        if(posicao == 2){
            return resultado;
        }
        if (dia) {
            resultado = resultado + tempo[2] + (tempo[2] > 1 ? " dias" : " dia");
        }
        return resultado;
    }
    
   
    public static LocalDateTime pegaLocalDateTime(){
        String dataBruta = Funcoes.getDataHora();
        String ano = dataBruta.substring(6, 10);
        String mes = dataBruta.substring(3, 5);
        String dia = dataBruta.substring(0, 2);
        String hora = dataBruta.substring(11, 13);
        String min = dataBruta.substring(14, 16);
        String seg = dataBruta.substring(17, 19);

        LocalDateTime dateTime = LocalDateTime.parse(ano+"-"+mes+"-"+dia+"T"+hora+":"+min+":"+seg);
        return dateTime;
    }
    
    public static LocalDateTime formataLocalDateTime(String date){
        String dataBruta = date;
        String ano = dataBruta.substring(6, 10);
        String mes = dataBruta.substring(3, 5);
        String dia = dataBruta.substring(0, 2);
        String hora = dataBruta.substring(11, 13);
        String min = dataBruta.substring(14, 16);
        String seg = dataBruta.substring(17, 19);

        LocalDateTime dateTime = LocalDateTime.parse(ano+"-"+mes+"-"+dia+"T"+hora+":"+min+":"+seg);
        return dateTime;
    }
   
}