package Negocio;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jhonatan
 */
@Entity(name = "Investimento")
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idInvestimento")
    private int idInvestimento;
    @Column
    private int idInvestidor;
    @Column
    private int idRendaFixa;
    @Column
    private int idRendaVariavel;
    @Column
    private String valorInicial;
    @Column
    private String valorAtualizado;
    @Column
    private String valorVenda;
    @Column
    private Date dataInicio;
    @Column
    private String valorAviso;
    @Column
    private Date dataAviso;
    @Column
    private String porcentagemAviso;
    @Column
    private int idCorretora;
     
    public static final String PROP_PORCENTAGEMAVISO = "porcentagemAviso";

    public static final String PROP_IDCORRETORA = "idCorretora";

    public int getIdCorretora() {
        return idCorretora;
    }

    public void setIdCorretora(int idCorretora) {
        int oldIdCorretora = this.idCorretora;
        this.idCorretora = idCorretora;
        propertyChangeSupport.firePropertyChange(PROP_IDCORRETORA, oldIdCorretora, idCorretora);
    }

    public String getPorcentagemAviso() {
        return porcentagemAviso;
    }

    public void setPorcentagemAviso(String porcentagemAviso) {
        String oldPorcentagemAviso = this.porcentagemAviso;
        this.porcentagemAviso = porcentagemAviso;
        propertyChangeSupport.firePropertyChange(PROP_PORCENTAGEMAVISO, oldPorcentagemAviso, porcentagemAviso);
    }

    /**
     * Get the value of dataAviso
     *
     * @return the value of dataAviso
     */
    public Date getDataAviso() {
        return dataAviso;
    }

    /**
     * Set the value of dataAviso
     *
     * @param dataAviso new value of dataAviso
     */
    public void setDataAviso(Date dataAviso) {
        this.dataAviso = dataAviso;
    }

    public static final String PROP_VALORAVISO = "valorAviso";

    public String getValorAviso() {
        return valorAviso;
    }

    public void setValorAviso(String valorAviso) {
        String oldValorAviso = this.valorAviso;
        this.valorAviso = valorAviso;
        propertyChangeSupport.firePropertyChange(PROP_VALORAVISO, oldValorAviso, valorAviso);
    }

    public static final String PROP_IDRENDAFIXA = "idRendaFixa";

    public static final String PROP_DATAINICIO = "dataInicio";

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        Date oldDataInicio = this.dataInicio;
        this.dataInicio = dataInicio;
        propertyChangeSupport.firePropertyChange(PROP_DATAINICIO, oldDataInicio, dataInicio);
    }

    public static final String PROP_VALORVENDA = "valorVenda";

    public String getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(String valorVenda) {
        String oldValorVenda = this.valorVenda;
        this.valorVenda = valorVenda;
        propertyChangeSupport.firePropertyChange(PROP_VALORVENDA, oldValorVenda, valorVenda);
    }

    public static final String PROP_VALORATUALIZADO = "valorAtualizado";

    public String getValorAtualizado() {
        return valorAtualizado;
    }

    public void setValorAtualizado(String valorAtualizado) {
        String oldValorAtualizado = this.valorAtualizado;
        this.valorAtualizado = valorAtualizado;
        propertyChangeSupport.firePropertyChange(PROP_VALORATUALIZADO, oldValorAtualizado, valorAtualizado);
    }

    public static final String PROP_VALORINICIAL = "valorInicial";

    public String getValorInicial() {
        return valorInicial;
    }

    public void setValorInicial(String valorInicial) {
        String oldValorInicial = this.valorInicial;
        this.valorInicial = valorInicial;
        propertyChangeSupport.firePropertyChange(PROP_VALORINICIAL, oldValorInicial, valorInicial);
    }

    /**
     * Get the value of idRendaVariavel
     *
     * @return the value of idRendaVariavel
     */
    public int getIdRendaVariavel() {
        return idRendaVariavel;
    }

    /**
     * Set the value of idRendaVariavel
     *
     * @param idRendaVariavel new value of idRendaVariavel
     */
    public void setIdRendaVariavel(int idRendaVariavel) {
        this.idRendaVariavel = idRendaVariavel;
    }

    public int getIdRendaFixa() {
        return idRendaFixa;
    }

    public void setIdRendaFixa(int idRendaFixa) {
        int oldIdRendaFixa = this.idRendaFixa;
        this.idRendaFixa = idRendaFixa;
        propertyChangeSupport.firePropertyChange(PROP_IDRENDAFIXA, oldIdRendaFixa, idRendaFixa);
    }

    /**
     * Get the value of idInvestidor
     *
     * @return the value of idInvestidor
     */
    public int getIdInvestidor() {
        return idInvestidor;
    }

    /**
     * Set the value of idInvestidor
     *
     * @param idInvestidor new value of idInvestidor
     */
    public void setIdInvestidor(int idInvestidor) {
        this.idInvestidor = idInvestidor;
    }

    public static final String PROP_IDINVESTIMENTO = "idInvestimento";

    public int getIdInvestimento() {
        return idInvestimento;
    }

    public void setIdInvestimento(int idInvestimento) {
        int oldIdInvestimento = this.idInvestimento;
        this.idInvestimento = idInvestimento;
        propertyChangeSupport.firePropertyChange(PROP_IDINVESTIMENTO, oldIdInvestimento, idInvestimento);
    }

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
}
