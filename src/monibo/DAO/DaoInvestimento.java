/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monibo.DAO;

import Negocio.Investimento;
import Servico.Manager;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
/**
 *
 * @author Jhonatan
 */
public class DaoInvestimento {
     private EntityManager em;

    public DaoInvestimento() {
        em = Manager.getInstance().getEm();
    }

    public boolean salvar(Investimento i) {
        try {
            em.getTransaction().begin();
            if (i.getIdInvestimento() > 0) {
                i = em.merge(i);
            }
            em.persist(i);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            em.getTransaction().rollback();
            return false;
        }
    }

    public List<Investimento> buscaTudo() {
        String sql = " SELECT i FROM Negocio.Investimento i ";
        Query query = em.createQuery(sql);
        List resultList = query.getResultList();
        return (List<Investimento>) resultList;
    }

    public void remover(Investimento i) {
        try {
            em.getTransaction().begin();
            em.remove(i);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }

    }

   
}
