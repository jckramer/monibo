CREATE TABLE investidor(
idInvestidor SERIAL PRIMARY KEY NOT NULL,
nome VARCHAR(200), -- NOME DO INVESTIDOR
login VARCHAR(200), -- LOGIN DO INVESTIDOR
senha VARCHAR(200), -- SENHA DO INVESTIDOR
dataNascimento DATE -- DATA NASIMENTO DO INVESTIDOR
);
create sequence idInvestidor increment 1 minvalue 1 maxvalue 9999999999 start 1 cache 1;

CREATE TABLE rendaVariavel(
idRendaVariavel SERIAL PRIMARY KEY NOT NULL,
abreviacao VARCHAR(10), -- ABREVIACAO OU SIGLA DA RENDA
descricao VARCHAR(200), -- DESCRICAO DA EMPRESA 
valorAbertura VARCHAR(200), -- VALOR DA ABERTURA DA ACAO
valorAtual VARCHAR(200), -- VALOR ATUALDA ACAO
valorFechamento VARCHAR(200), -- VALOR DO FECHAMENTO DA ACAO
porcentagem VARCHAR (200) -- PORCENTAGEM ENTRE FECHAMENTO E ABERTURA
);
create sequence idRendaVariavel increment 1 minvalue 1 maxvalue 9999999999 start 1 cache 1;

CREATE TABLE rendaFixa(
idRendaFixa SERIAL PRIMARY KEY NOT NULL,
nome VARCHAR (200), -- NOME DA RENDA
rentabilidade VARCHAR(200), -- RENTABILIDADE DO INVESTIMENTO
PeriodoRentabilidade INT, -- PERIODO DE INVESTIMENTO EM DIAS
iof VARCHAR(200), -- VALOR DO IOF
periodo INT, -- PERIODO DO IOF EM DIAS
impostoRenda VARCHAR (200), -- PORCENTAGEM DO IMPOSTO DE RENDA
tipo VARCHAR(200) -- TIPO DA ACAO EX CDB, CDI, TESOURO ...
);
create sequence idRendaFixa increment 1 minvalue 1 maxvalue 9999999999 start 1 cache 1;

CREATE TABLE corretora(
idCorretora SERIAL PRIMARY KEY NOT NULL,
nome VARCHAR(200) -- NOME DA CORRETORA
);
create sequence idCorretora increment 1 minvalue 1 maxvalue 9999999999 start 1 cache 1;

CREATE TABLE investimento(
idInvestimento SERIAL PRIMARY KEY NOT NULL,
idInvestidor INT NOT NULL,
idRendaFixa INT,
idRendaVariavel INT,
idCorretora INT NOT NULL,
valorInicial VARCHAR(200), -- VALOR DO INVESTMENTO INICIAL
valorAtualizado VARCHAR(200), -- VALOR DO INVESTIMENTO NO MOMENTO
valorVenda VARCHAR(200), -- VALOR DA VENDA OU RESGATE DO INVESTIMENTO
dataInicio DATE, -- DATA INICIAL DO INVESTIMENTO
valorAviso VARCHAR(200), -- AO ATINGIR VALOR MANDAR AVISO PARA O CLIENTE
porcentagemAviso VARCHAR(10), -- PORCENTAGEM AO SER ATINGIDA ENVIAR AVISO
dataAviso DATE -- NA DATA ENVIAR AVISO

);
create sequence idInvestimento increment 1 minvalue 1 maxvalue 9999999999 start 1 cache 1;

CREATE TABLE auditoria(
idAuditoria  SERIAL PRIMARY KEY NOT NULL,
tabela VARCHAR(200),
valorOriginal VARCHAR(1000),
valorMudança VARCHAR(1000),
dataModificacao DATE
);
create sequence idAuditoria increment 1 minvalue 1 maxvalue 9999999999 start 1 cache 1;
